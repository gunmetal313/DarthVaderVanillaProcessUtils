﻿namespace OpenHack
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPID = new System.Windows.Forms.ComboBox();
            this.dataObjectList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDebugOutput = new System.Windows.Forms.TextBox();
            this.btnInitHacks = new System.Windows.Forms.Button();
            this.btnExample1 = new System.Windows.Forms.Button();
            this.btnExample2 = new System.Windows.Forms.Button();
            this.btnExample3 = new System.Windows.Forms.Button();
            this.btnExample4 = new System.Windows.Forms.Button();
            this.btnUpdateObjects = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataObjectList)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbPID
            // 
            this.cmbPID.FormattingEnabled = true;
            this.cmbPID.Location = new System.Drawing.Point(3, 1);
            this.cmbPID.Name = "cmbPID";
            this.cmbPID.Size = new System.Drawing.Size(121, 21);
            this.cmbPID.TabIndex = 0;
            this.cmbPID.SelectedIndexChanged += new System.EventHandler(this.cmbPID_SelectedIndexChanged);
            this.cmbPID.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbPID_MouseDown);
            // 
            // dataObjectList
            // 
            this.dataObjectList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataObjectList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataObjectList.Location = new System.Drawing.Point(3, 28);
            this.dataObjectList.Name = "dataObjectList";
            this.dataObjectList.Size = new System.Drawing.Size(643, 630);
            this.dataObjectList.TabIndex = 1;
            this.dataObjectList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataObjectList_CellClick);
            this.dataObjectList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataObjectList_CellDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Type";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "BaseAddr";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Name";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "X";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Y";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Z";
            this.Column6.Name = "Column6";
            // 
            // txtDebugOutput
            // 
            this.txtDebugOutput.Location = new System.Drawing.Point(653, 28);
            this.txtDebugOutput.Multiline = true;
            this.txtDebugOutput.Name = "txtDebugOutput";
            this.txtDebugOutput.Size = new System.Drawing.Size(397, 630);
            this.txtDebugOutput.TabIndex = 2;
            // 
            // btnInitHacks
            // 
            this.btnInitHacks.Location = new System.Drawing.Point(131, 1);
            this.btnInitHacks.Name = "btnInitHacks";
            this.btnInitHacks.Size = new System.Drawing.Size(75, 23);
            this.btnInitHacks.TabIndex = 3;
            this.btnInitHacks.Text = "Init Hacks";
            this.btnInitHacks.UseVisualStyleBackColor = true;
            this.btnInitHacks.Click += new System.EventHandler(this.btnInitHacks_Click);
            // 
            // btnExample1
            // 
            this.btnExample1.Location = new System.Drawing.Point(293, 1);
            this.btnExample1.Name = "btnExample1";
            this.btnExample1.Size = new System.Drawing.Size(75, 23);
            this.btnExample1.TabIndex = 4;
            this.btnExample1.Text = "CollectHerbs";
            this.btnExample1.UseVisualStyleBackColor = true;
            this.btnExample1.Click += new System.EventHandler(this.btnExample1_Click);
            // 
            // btnExample2
            // 
            this.btnExample2.Location = new System.Drawing.Point(374, 1);
            this.btnExample2.Name = "btnExample2";
            this.btnExample2.Size = new System.Drawing.Size(75, 23);
            this.btnExample2.TabIndex = 5;
            this.btnExample2.Text = "SpeedHack";
            this.btnExample2.UseVisualStyleBackColor = true;
            this.btnExample2.Click += new System.EventHandler(this.btnExample2_Click);
            // 
            // btnExample3
            // 
            this.btnExample3.Location = new System.Drawing.Point(455, 1);
            this.btnExample3.Name = "btnExample3";
            this.btnExample3.Size = new System.Drawing.Size(75, 23);
            this.btnExample3.TabIndex = 6;
            this.btnExample3.Text = "Example3";
            this.btnExample3.UseVisualStyleBackColor = true;
            // 
            // btnExample4
            // 
            this.btnExample4.Location = new System.Drawing.Point(536, 1);
            this.btnExample4.Name = "btnExample4";
            this.btnExample4.Size = new System.Drawing.Size(75, 23);
            this.btnExample4.TabIndex = 7;
            this.btnExample4.Text = "Example4";
            this.btnExample4.UseVisualStyleBackColor = true;
            // 
            // btnUpdateObjects
            // 
            this.btnUpdateObjects.Location = new System.Drawing.Point(212, 1);
            this.btnUpdateObjects.Name = "btnUpdateObjects";
            this.btnUpdateObjects.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateObjects.TabIndex = 8;
            this.btnUpdateObjects.Text = "GetObjects";
            this.btnUpdateObjects.UseVisualStyleBackColor = true;
            this.btnUpdateObjects.Click += new System.EventHandler(this.btnUpdateObjects_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 659);
            this.Controls.Add(this.btnUpdateObjects);
            this.Controls.Add(this.btnExample4);
            this.Controls.Add(this.btnExample3);
            this.Controls.Add(this.btnExample2);
            this.Controls.Add(this.btnExample1);
            this.Controls.Add(this.btnInitHacks);
            this.Controls.Add(this.txtDebugOutput);
            this.Controls.Add(this.dataObjectList);
            this.Controls.Add(this.cmbPID);
            this.Name = "frmMain";
            this.Text = "daCoders Tool for WoW 1.12.1";
            ((System.ComponentModel.ISupportInitialize)(this.dataObjectList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPID;
        private System.Windows.Forms.DataGridView dataObjectList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.TextBox txtDebugOutput;
        private System.Windows.Forms.Button btnInitHacks;
        private System.Windows.Forms.Button btnExample1;
        private System.Windows.Forms.Button btnExample2;
        private System.Windows.Forms.Button btnExample3;
        private System.Windows.Forms.Button btnExample4;
        private System.Windows.Forms.Button btnUpdateObjects;
    }
}

