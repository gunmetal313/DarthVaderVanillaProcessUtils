﻿/*
Copyright 2012 DaCoder
This file is part of daCoders Tool for WoW 1.12.1.
daCoders Tool for WoW 1.12.1 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
daCoders Tool for WoW 1.12.1 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenHack
{
    class Pointers
    {

        #region Nested type: StaticPointers
        /// <summary>
        ///   1.12.1.5875
        /// </summary>
        internal enum StaticPointers
        {
            CurrentTargetGUID = 0x74e2d8,
            LocalPlayerGUID = 0x741e30,
        }

        #endregion

        #region Nested type: ObjectManager
        /// <summary>
        ///   1.12.1.5875
        /// </summary>
        internal enum ObjectManager
        {
            CurMgrPointer = 0x00741414,
            CurMgrOffset = 0xAC,
            NextObject = 0x3c,
            FirstObject = 0xAC,
            LocalGUID = 0xC0,
        }

        #endregion


        #region Nested type: UnitName
        /// <summary>
        ///    1.12.1.5875
        /// </summary>

        internal enum UnitName : uint
        {
            ObjectName1 = 0x214, //pointing to itemtype of objectdescription
            ItemType = 0x2DC, // *DataPTR (0x288) + 0x54
            ObjectName2 = 0x8,
            UnitName1 = 0xB30, 
            UnitName2 = 0x0,

            PlayerNameCachePointer = 0x80E230, 
            PlayerNameGUIDOffset = 0xc,
            PlayerNameStringOffset = 0x14
        }

        #endregion

        #region Nested type: WowObject

        /// <summary>
        ///   1.12.1.5875
        /// </summary>
        internal enum WowObject
        {
            DataPTR = 0x8,
            Type = 0x14,
            Guid = 0x30, 
            Y = 0x9b8, 
            X = Y + 0x4,
            Z = Y + 0x8, 
            RotationOffset = X + 0x10,  // This seems to be wrong
            GameObjectY = 0x2C4, // *DataPTR (0x288) + 0x3c
            GameObjectX = GameObjectY + 0x4, 
            GameObjectZ = GameObjectY + 0x8,
            Speed = 0xA34
        }

        #endregion

       
    }
}
